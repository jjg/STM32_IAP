/*******************************************************************************
* File Name          : Main.c
* Author             : 叫我小陆吧
* Version            : V1.0
* Date               : 2015.08.19
* Description        : QQ:260869626
********************************************************************************
* 
* 
* 
*
* 
*******************************************************************************/
/* Includes ------------------------------------------------------------------*/

#include "stm32f10x.h"	
#include "bsp_int.h"
#include <string.h>
#include <AES.h>
#include "hw_config.h"
#include "usb_pwr.h"

  
__align(4) u8  FLASH_TMP_BUF[IN_FLASH_SECTOR];                     /* bin文件缓存，一个扇区 */
u8 *P_CHK=(u8*)IN_FLASH_STAR; 		 		/* flash查空指针 */
user_app Jump_To_Application;		 		/* 跳转函数指针 */
u32 JumpAddress;					/* 跳转地址 */
u8  KEY[32]="1234567890ABCDEFGHIJabcdefghiLHD";	

void delay(u32 i)
{
	u32 j=100;
	while(i--)
	{
		while(j--);
		j=100;
	}
	
}

/*******************************************************************************
* Function Name  : main
* Description    : Main program.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
extern void Txt_Write_Update_Finish(void);
extern u16 MAL_Init(u8 lun);
u8 RUN_FLAG=0;
int main(void)
{
  u32 led=0;

  INIT();
	delay(1);
	USB_HW_INIT();
	delay(200000);
  //读usb_boot启动引脚
  //将正确的升级文件放入U盘即可完成app程序更新
//  if( GPIO_ReadInputDataBit(BOOT_DISCONNECT, BOOT_DISCONNECT_PIN)==1 )

	if(bDeviceState!=CONFIGURED)
  {
		//start_app:;
      FLASH_Lock();
      GPIO_ResetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);  
      RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB,DISABLE);

      __asm("CPSID   I");//关中断
      GPIO_DeInit(GPIOA);
      GPIO_DeInit(GPIOB);
      GPIO_DeInit(GPIOC);
      GPIO_DeInit(GPIOD);
      GPIO_DeInit(GPIOE);

      if (((*(vu32*)IN_FLASH_STAR) & 0x2FFE0000 ) == 0x20000000)
      {
               JumpAddress = *(vu32*) (IN_FLASH_STAR+4);
               Jump_To_Application = (user_app) JumpAddress;
               __set_MSP(*(vu32*) IN_FLASH_STAR );
               Jump_To_Application();
      }     
      *((u32 *)0xE000ED0C) = 0x05fa0004;     
  }
  while(1)
 {
	 led++;
	 if(led==200000)
	 {
		 GPIOC->ODR^=(1<<12);
	 }
	 if(led==210000)
	 {
		 GPIOC->ODR^=(1<<12);
	 }
	 
	 if(led>=1000000)
	 {
		 led=0;
		 if(RUN_FLAG)//更新完成重新枚举
		 {
			RUN_FLAG=0;
			delay(100000); 
			 RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB,DISABLE);
			GPIO_ResetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);
			delay(1000);
			 
			GPIO_SetBits(USB_DISCONNECT, USB_DISCONNECT_PIN);
			RCC_APB1PeriphClockCmd(RCC_APB1Periph_USB,ENABLE);
			MAL_Init(0);
			Txt_Write_Update_Finish();
			 //goto start_app;
		 }
	 }
 }
    
}



/************************* *************END OF FILE****/

