#ifndef _delay_h_
#define _delay_h_

/**********************************************************
                     外部函数头文件                        
**********************************************************/

#include "sys.h"

void delay_us(u16 time);	//延时n个us
void delay_ms(u16 time);	//延时n个ms

#endif
