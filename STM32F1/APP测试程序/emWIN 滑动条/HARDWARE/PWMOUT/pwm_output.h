#ifndef __PWM_OUTPUT_H
#define	__PWM_OUTPUT_H

#include "stm32f10x.h"

void TIM4_PWM_Init(void);


extern u16 CCR1_Val,CCR2_Val,CCR3_Val;

#endif /* __PWM_OUTPUT_H */

