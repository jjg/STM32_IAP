
#include "ILI9481_TFT.h"
#include "stdlib.h"
#include "font.h" 
#include "usart.h"	 
#include "delay.h"	   
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//2.4/2.8寸/3.5寸 TFT液晶驱动	  
//支持驱动IC型号包括:ILI9341/ILI9325/RM68042/RM68021/ILI9320/ILI9328/LGDP4531/LGDP4535/SPFD5408/SSD1289/1505/B505/C505等	    
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/10/7
//版本：V2.2
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved	
//********************************************************************************
//V1.2修改说明
//支持了SPFD5408的驱动,另外把液晶ID直接打印成HEX格式.方便查看ILI9481_TFT驱动IC.
//V1.3
//加入了快速IO的支持
//修改了背光控制的极性（适用于V1.8及以后的开发板版本）
//对于1.8版本之前(不包括1.8)的液晶模块,请修改ILI9481_TFT_Init函数的ILI9481_TFT_LED=1;为ILI9481_TFT_LED=1;
//V1.4
//修改了ILI9481_TFT_ShowChar函数，使用画点功能画字符。
//加入了横竖屏显示的支持
//V1.5 20110730
//1,修改了B505液晶读颜色有误的bug.
//2,修改了快速IO及横竖屏的设置方式.
//V1.6 20111116
//1,加入对LGDP4535液晶的驱动支持
//V1.7 20120713
//1,增加ILI9481_TFT_RD_DATA函数
//2,增加对ILI9341的支持
//3,增加ILI9325的独立驱动代码
//4,增加ILI9481_TFT_Scan_Dir函数(慎重使用)	  
//6,另外修改了部分原来的函数,以适应9341的操作
//V1.8 20120905
//1,加入ILI9481_TFT重要参数设置结构体ILI9481_TFTdev
//2,加入ILI9481_TFT_Display_Dir函数,支持在线横竖屏切换
//V1.9 20120911
//1,新增RM68042驱动（ID:6804），但是6804不支持横屏显示！！原因：改变扫描方式，
//导致6804坐标设置失效，试过很多方法都不行，暂时无解。
//V2.0 20120924
//在不硬件复位的情况下,ILI9341的ID读取会被误读成9300,修改ILI9481_TFT_Init,将无法识别
//的情况（读到ID为9300/非法ID）,强制指定驱动IC为ILI9341，执行9341的初始化。
//V2.1 20120930
//修正ILI9325读颜色的bug。
//V2.2 20121007
//修正ILI9481_TFT_Scan_Dir的bug。
//////////////////////////////////////////////////////////////////////////////////	 
				 
//ILI9481_TFT的画笔颜色和背景色	   
u16 POINT_COLOR=0x0000;	//画笔颜色
u16 BACK_COLOR=0xFFFF;  //背景色 

//管理ILI9481_TFT重要参数
//默认为竖屏
_ILI9481_TFT_dev ILI9481_TFTdev;
	
		   
//写寄存器函数
//regval:寄存器值
void ILI9481_TFT_WR_REG(u16 regval)
{ 
	ILI9481_TFT->ILI9481_TFT_REG=regval;//写入要写的寄存器序号	 
}
//写ILI9481_TFT数据
//data:要写入的值
void ILI9481_TFT_WR_DATA(u16 data)
{										    	   
	ILI9481_TFT->ILI9481_TFT_RAM=data;		 
}
//读ILI9481_TFT数据
//返回值:读到的值
u16 ILI9481_TFT_RD_DATA(void)
{										    	   
	return ILI9481_TFT->ILI9481_TFT_RAM;		 
}					   
//写寄存器
//ILI9481_TFT_Reg:寄存器地址
//ILI9481_TFT_RegValue:要写入的数据
void ILI9481_TFT_WriteReg(u8 ILI9481_TFT_Reg, u16 ILI9481_TFT_RegValue)
{	
	ILI9481_TFT->ILI9481_TFT_REG = ILI9481_TFT_Reg;		//写入要写的寄存器序号	 
	ILI9481_TFT->ILI9481_TFT_RAM = ILI9481_TFT_RegValue;//写入数据	    		 
}	   
//读寄存器
//ILI9481_TFT_Reg:寄存器地址
//返回值:读到的数据
u16 ILI9481_TFT_ReadReg(u8 ILI9481_TFT_Reg)
{										   
	ILI9481_TFT_WR_REG(ILI9481_TFT_Reg);		//写入要读的寄存器序号
	delay_us(5);		  
	return ILI9481_TFT_RD_DATA();		//返回读到的值
}   
//开始写GRAM
void ILI9481_TFT_WriteRAM_Prepare(void)
{
 	ILI9481_TFT->ILI9481_TFT_REG=ILI9481_TFTdev.wramcmd;	  
}	 
//ILI9481_TFT写GRAM
//RGB_Code:颜色值
void ILI9481_TFT_WriteRAM(u16 RGB_Code)
{							    
	ILI9481_TFT->ILI9481_TFT_RAM = RGB_Code;//写十六位GRAM
}
//从ILI93xx读出的数据为GBR格式，而我们写入的时候为RGB格式。
//通过该函数转换
//c:GBR格式的颜色值
//返回值：RGB格式的颜色值
u16 ILI9481_TFT_BGR2RGB(u16 c)
{
	u16  r,g,b,rgb;   
	r=(c>>0)&0x1f;
	g=(c>>5)&0x3f;
	b=(c>>11)&0x1f;	 
	rgb=(r<<11)+(g<<5)+(b<<0);		 
	return(rgb);
} 

//当mdk -O1时间优化时需要设置
//延时i
void opt_delay(u8 i)
{
	while(i--);
}
//读取个某点的颜色值	 
//x,y:坐标
//返回值:此点的颜色
u16 ILI9481_TFT_ReadPoint(void)
{
   static u16  r=0;
   //if(x>=ILI9481_TFTdev.width||y>=ILI9481_TFTdev.height)return 0;	//超过了范围,直接返回		   
   //ILI9481_TFT_SetCursor(x,y);	    
   ILI9481_TFT_WR_REG(0X2E);// 发送读GRAM指令 
   //if(ILI9481_TFT->ILI9481_TFT_RAM)r=0;							//dummy Read	     
   r=ILI9481_TFT->ILI9481_TFT_RAM;  		  						//实际坐标颜色
   return r;	 											//其他IC
}			 
//ILI9481_TFT开启显示
void ILI9481_TFT_DisplayOn(void)
{					   
	if(ILI9481_TFTdev.id==0X9341||ILI9481_TFTdev.id==0X6804)ILI9481_TFT_WR_REG(0X29);	//开启显示
	else ILI9481_TFT_WriteReg(R7,0x0173); 			//开启显示
}	 
//ILI9481_TFT关闭显示
void ILI9481_TFT_DisplayOff(void)
{	   
	if(ILI9481_TFTdev.id==0X9341||ILI9481_TFTdev.id==0X6804)ILI9481_TFT_WR_REG(0X28);	//关闭显示
	else ILI9481_TFT_WriteReg(R7,0x0);//关闭显示 
}   
//设置光标位置
//Xpos:横坐标
//Ypos:纵坐标
void ILI9481_TFT_SetCursor(u16 Xpos, u16 Ypos)
{	 
 	if(ILI9481_TFTdev.id==0X9341||ILI9481_TFTdev.id==0X6804||ILI9481_TFTdev.id==0X9481)
	{		    
		ILI9481_TFT_WR_REG(ILI9481_TFTdev.setxcmd); 
		ILI9481_TFT_WR_DATA(Xpos>>8); 
		ILI9481_TFT_WR_DATA(Xpos&0XFF);	 
		ILI9481_TFT_WR_REG(ILI9481_TFTdev.setycmd); 
		ILI9481_TFT_WR_DATA(Ypos>>8); 
		ILI9481_TFT_WR_DATA(Ypos&0XFF);
	}else
	{
		if(ILI9481_TFTdev.dir==1)Xpos=ILI9481_TFTdev.width-1-Xpos;//横屏其实就是调转x,y坐标
		ILI9481_TFT_WriteReg(ILI9481_TFTdev.setxcmd, Xpos);
		ILI9481_TFT_WriteReg(ILI9481_TFTdev.setycmd, Ypos);
	}	 
} 		 
//设置ILI9481_TFT的自动扫描方向
//注意:其他函数可能会受到此函数设置的影响(尤其是9341/6804这两个奇葩),
//所以,一般设置为L2R_U2D即可,如果设置为其他扫描方式,可能导致显示不正常.
//dir:0~7,代表8个方向(具体定义见ILI9481_TFT.h)
//9320/9325/9328/4531/4535/1505/b505/8989/5408/9341等IC已经实际测试	   	   
void ILI9481_TFT_Scan_Dir(u8 dir)
{
	if(ILI9481_TFTdev.id == 0X9481)
	{
		switch(dir)
		{
			case L2R_U2D://从左到右,从上到下
				ILI9481_TFTdev.width=320;
				ILI9481_TFTdev.height=480;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2A;
				ILI9481_TFTdev.setycmd=0x2B;	
				ILI9481_TFT_WriteReg(0x36,0X02);
				break;
			case L2R_D2U://从左到右,从下到上
				ILI9481_TFTdev.width=320;
				ILI9481_TFTdev.height=480;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2A;
				ILI9481_TFTdev.setycmd=0x2B;	
				ILI9481_TFT_WriteReg(0x36,0X03); 
				break;
			case R2L_U2D://从右到左,从上到下
				ILI9481_TFTdev.width=320;
				ILI9481_TFTdev.height=480;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2A;
				ILI9481_TFTdev.setycmd=0x2B;	
				ILI9481_TFT_WriteReg(0x36,0X00);
				break;
			case R2L_D2U://从右到左,从下到上
				ILI9481_TFTdev.width=320;
				ILI9481_TFTdev.height=480;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2A;
				ILI9481_TFTdev.setycmd=0x2B;	
				ILI9481_TFT_WriteReg(0x36,0X01);
				break;	 
			case U2D_L2R://从上到下,从左到右
				ILI9481_TFTdev.width=480;
				ILI9481_TFTdev.height=320;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2B;
				ILI9481_TFTdev.setycmd=0x2A;	
				ILI9481_TFT_WriteReg(0x36,0X02);
				break;
			case U2D_R2L://从上到下,从右到左
				ILI9481_TFTdev.width=480;
				ILI9481_TFTdev.height=320;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2B;
				ILI9481_TFTdev.setycmd=0x2A;	
				ILI9481_TFT_WriteReg(0x36,0X00);
				break;
			case D2U_L2R://从下到上,从左到右
				ILI9481_TFTdev.width=480;
				ILI9481_TFTdev.height=320;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2B;
				ILI9481_TFTdev.setycmd=0x2A;	
				ILI9481_TFT_WriteReg(0x36,0X03);
				break;
			case D2U_R2L://从下到上,从右到左
				ILI9481_TFTdev.width=480;
				ILI9481_TFTdev.height=320;
				ILI9481_TFTdev.wramcmd=0x2C;
				ILI9481_TFTdev.setxcmd=0x2B;
				ILI9481_TFTdev.setycmd=0x2A;	
				ILI9481_TFT_WriteReg(0x36,0X01); 
				break;	 
		}
	
	}
}   
//画点
//x,y:坐标
//POINT_COLOR:此点的颜色
void ILI9481_TFT_DrawPoint(u16 x,u16 y)
{
	ILI9481_TFT_SetCursor(x,y);		//设置光标位置 
	ILI9481_TFT_WriteRAM_Prepare();	//开始写入GRAM
	ILI9481_TFT->ILI9481_TFT_RAM=POINT_COLOR; 
}
//快速画点
//x,y:坐标
//color:颜色
void ILI9481_TFT_Fast_DrawPoint(u16 x,u16 y,u16 color)
{	   
	if(ILI9481_TFTdev.id==0X9341||ILI9481_TFTdev.id==0X6804||ILI9481_TFTdev.id==0X9481)
	{		    
		ILI9481_TFT_WR_REG(ILI9481_TFTdev.setxcmd); 
		ILI9481_TFT_WR_DATA(x>>8); 
		ILI9481_TFT_WR_DATA(x&0XFF);	 
		ILI9481_TFT_WR_REG(ILI9481_TFTdev.setycmd); 
		ILI9481_TFT_WR_DATA(y>>8); 
		ILI9481_TFT_WR_DATA(y&0XFF);
	}else
	{
 		if(ILI9481_TFTdev.dir==1)x=ILI9481_TFTdev.width-1-x;//横屏其实就是调转x,y坐标
		ILI9481_TFT_WriteReg(ILI9481_TFTdev.setxcmd,x);
		ILI9481_TFT_WriteReg(ILI9481_TFTdev.setycmd,y);
	}			 
	ILI9481_TFT->ILI9481_TFT_REG=ILI9481_TFTdev.wramcmd; 
	ILI9481_TFT->ILI9481_TFT_RAM=color; 
}	 
//设置ILI9481_TFT显示方向（6804不支持横屏显示）
//dir:0,竖屏；1,横屏
void ILI9481_TFT_Display_Dir(u8 dir)
{
	if(dir==0)//竖屏
	{
			ILI9481_TFTdev.dir=0;//竖屏				 	 		
			ILI9481_TFTdev.width=320;
			ILI9481_TFTdev.height=480;
			ILI9481_TFTdev.wramcmd=0x2C;
	 		ILI9481_TFTdev.setxcmd=0x2A;
			ILI9481_TFTdev.setycmd=0x2B;	
			ILI9481_TFT_WriteReg(0x36,0x0A);

	}
	else 	
	{	  
			ILI9481_TFTdev.dir=1;//横屏
			ILI9481_TFTdev.width=480;
			ILI9481_TFTdev.height=320;	
			ILI9481_TFTdev.wramcmd=0x2C;
	 		ILI9481_TFTdev.setxcmd=0x2B;
			ILI9481_TFTdev.setycmd=0x2A;	
			ILI9481_TFT_WriteReg(0x36,0x08);
	} 
	ILI9481_TFT_Scan_Dir(L2R_U2D);
} 
//初始化ILI9481_TFT
//该初始化函数可以初始化各种ILI93XX液晶,但是其他函数是基于ILI9320的!!!
//在其他型号的驱动芯片上没有测试! 
void ILI9481_TFT_Init(void)
{ 										  
   GPIO_InitTypeDef GPIO_InitStructure;
   FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
   FSMC_NORSRAMTimingInitTypeDef  readWriteTiming; 
   FSMC_NORSRAMTimingInitTypeDef  writeTiming;

   RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC,ENABLE);	//使能FSMC时钟
   RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOD|RCC_APB2Periph_GPIOE|RCC_APB2Periph_GPIOG|RCC_APB2Periph_AFIO,ENABLE);//使能PORTB,D,E,G以及AFIO复用功能时钟

   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;				 //PB0 推挽输出 背光
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(GPIOB, &GPIO_InitStructure);
   ILI9481_TFT_LED =1;

   //PORTD复用推挽输出  
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_7|GPIO_Pin_11|GPIO_Pin_1|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_14|GPIO_Pin_15;				 //	//PORTD复用推挽输出  
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		 //复用推挽输出   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(GPIOD, &GPIO_InitStructure); 

   //PORTE复用推挽输出  
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;				 //	//PORTD复用推挽输出  
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		 //复用推挽输出   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(GPIOE, &GPIO_InitStructure); 

   //	//PORTG12复用推挽输出 A0	
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_12;	 //	//PORTD复用推挽输出  
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP; 		 //复用推挽输出   
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_Init(GPIOG, &GPIO_InitStructure); 

   readWriteTiming.FSMC_AddressSetupTime = 0x01;	 //地址建立时间（ADDSET）为2个HCLK 1/36M=27ns
   readWriteTiming.FSMC_AddressHoldTime = 0x00;	 //地址保持时间（ADDHLD）模式A未用到	
   readWriteTiming.FSMC_DataSetupTime = 0x0f;		 // 数据保存时间为16个HCLK,因为液晶驱动IC的读数据的时候，速度不能太快，尤其对1289这个IC。
   readWriteTiming.FSMC_BusTurnAroundDuration = 0x00;
   readWriteTiming.FSMC_CLKDivision = 0x00;
   readWriteTiming.FSMC_DataLatency = 0x00;
   readWriteTiming.FSMC_AccessMode = FSMC_AccessMode_A;	 //模式A 


   writeTiming.FSMC_AddressSetupTime = 0x01;	 //0x01 地址建立时间（ADDSET）为1个HCLK  
   writeTiming.FSMC_AddressHoldTime = 0x00;	 //地址保持时间（A		
   writeTiming.FSMC_DataSetupTime = 0x02;		 ////0x03 数据保存时间为4个HCLK	
   writeTiming.FSMC_BusTurnAroundDuration = 0x00;
   writeTiming.FSMC_CLKDivision = 0x00;
   writeTiming.FSMC_DataLatency = 0x00;
   writeTiming.FSMC_AccessMode = FSMC_AccessMode_A;	 //模式A 


   FSMC_NORSRAMInitStructure.FSMC_Bank = FSMC_Bank1_NORSRAM1;//  这里我们使用NE4 ，也就对应BTCR[6],[7]。
   FSMC_NORSRAMInitStructure.FSMC_DataAddressMux = FSMC_DataAddressMux_Disable; // 不复用数据地址
   FSMC_NORSRAMInitStructure.FSMC_MemoryType =FSMC_MemoryType_SRAM;// FSMC_MemoryType_SRAM;  //SRAM   
   FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth = FSMC_MemoryDataWidth_16b;//存储器数据宽度为16bit   
   FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode =FSMC_BurstAccessMode_Disable;// FSMC_BurstAccessMode_Disable; 
   FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity = FSMC_WaitSignalPolarity_Low;
   FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait=FSMC_AsynchronousWait_Disable; 
   FSMC_NORSRAMInitStructure.FSMC_WrapMode = FSMC_WrapMode_Disable;   
   FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive = FSMC_WaitSignalActive_BeforeWaitState;  
   FSMC_NORSRAMInitStructure.FSMC_WriteOperation = FSMC_WriteOperation_Enable;	//  存储器写使能
   FSMC_NORSRAMInitStructure.FSMC_WaitSignal = FSMC_WaitSignal_Disable;   
   FSMC_NORSRAMInitStructure.FSMC_ExtendedMode = FSMC_ExtendedMode_Enable; // 读写使用不同的时序
   FSMC_NORSRAMInitStructure.FSMC_WriteBurst = FSMC_WriteBurst_Disable; 
   FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct = &readWriteTiming; //读写时序
   FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct = &writeTiming;  //写时序

   FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);  //初始化FSMC配置

   FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1,ENABLE);  // 使能BANK1
		
	delay_ms(50); // delay 50 ms 
	ILI9481_TFT_WriteReg(0x0000,0x0001);
	delay_ms(50); // delay 50 ms 
	//ILI9481读ID操作
	ILI9481_TFT_WR_REG(0XBF);				   
	ILI9481_TFT_RD_DATA(); 			//dummy read 	 
	ILI9481_TFT_RD_DATA();   	    //读回0X01			   
	ILI9481_TFT_RD_DATA(); 			//读回0XD0 			  	
	ILI9481_TFTdev.id=ILI9481_TFT_RD_DATA();//这里读回0X68 
	ILI9481_TFTdev.id<<=8;
	ILI9481_TFTdev.id|=ILI9481_TFT_RD_DATA();//这里读回0X04	   	  
	printf(" ILI9481_TFT ID:%x\r\n",ILI9481_TFTdev.id); //打印ILI9481_TFT ID    
  	if(ILI9481_TFTdev.id<0XFF||ILI9481_TFTdev.id==0XFFFF||ILI9481_TFTdev.id==0X9300)//读到ID不正确,新增ILI9481_TFTdev.id==0X9300判断，因为9341在未被复位的情况下会被读成9300
	{	
 		//尝试9341 ID的读取		
		ILI9481_TFT_WR_REG(0XD3);				   
		ILI9481_TFT_RD_DATA(); 				//dummy read 	
 		ILI9481_TFT_RD_DATA();   	    	//读到0X00
  		ILI9481_TFTdev.id=ILI9481_TFT_RD_DATA();   	//读取93								   
 		ILI9481_TFTdev.id<<=8;
		ILI9481_TFTdev.id|=ILI9481_TFT_RD_DATA();  	//读取41 	   			   
 		if(ILI9481_TFTdev.id!=0X9341)		//非9341,尝试是不是6804
		{	
 			ILI9481_TFT_WR_REG(0XBF);				   
			ILI9481_TFT_RD_DATA(); 			//dummy read 	 
	 		ILI9481_TFT_RD_DATA();   	    //读回0X01			   
	 		ILI9481_TFT_RD_DATA(); 			//读回0XD0 			  	
	  		ILI9481_TFTdev.id=ILI9481_TFT_RD_DATA();//这里读回0X68 
			ILI9481_TFTdev.id<<=8;
	  		ILI9481_TFTdev.id|=ILI9481_TFT_RD_DATA();//这里读回0X04	   	  
 		} 
		if(ILI9481_TFTdev.id!=0X9341&&ILI9481_TFTdev.id!=0X6804)ILI9481_TFTdev.id=0x9341;//新增，用于识别9341 	     
	}
 	printf(" ILI9481_TFT ID:%x\r\n",ILI9481_TFTdev.id); //打印ILI9481_TFT ID  
	if(ILI9481_TFTdev.id==0x9481)
	{	
			      
		ILI9481_TFT_WR_REG(0x11);
		delay_ms(20);
		ILI9481_TFT_WR_REG(0xD0);
		ILI9481_TFT_WR_DATA(0x07);
		ILI9481_TFT_WR_DATA(0x42);
		ILI9481_TFT_WR_DATA(0x18);
		
		ILI9481_TFT_WR_REG(0xD1);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x07);//07
		ILI9481_TFT_WR_DATA(0x10);
		
		ILI9481_TFT_WR_REG(0xD2);
		ILI9481_TFT_WR_DATA(0x01);
		ILI9481_TFT_WR_DATA(0x02);
		
		ILI9481_TFT_WR_REG(0xC0);
		ILI9481_TFT_WR_DATA(0x10);
		ILI9481_TFT_WR_DATA(0x3B);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x02);
		ILI9481_TFT_WR_DATA(0x11);
		
		ILI9481_TFT_WR_REG(0xC5);
		ILI9481_TFT_WR_DATA(0x03);
		
		ILI9481_TFT_WR_REG(0xC8);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x32);
		ILI9481_TFT_WR_DATA(0x36);
		ILI9481_TFT_WR_DATA(0x45);
		ILI9481_TFT_WR_DATA(0x06);
		ILI9481_TFT_WR_DATA(0x16);
		ILI9481_TFT_WR_DATA(0x37);
		ILI9481_TFT_WR_DATA(0x75);
		ILI9481_TFT_WR_DATA(0x77);
		ILI9481_TFT_WR_DATA(0x54);
		ILI9481_TFT_WR_DATA(0x0C);
		ILI9481_TFT_WR_DATA(0x00);
		
		ILI9481_TFT_WR_REG(0x36);
		ILI9481_TFT_WR_DATA(0x0A);
		
		ILI9481_TFT_WR_REG(0x3A);
		ILI9481_TFT_WR_DATA(0x55);
		
		ILI9481_TFT_WR_REG(0x2A);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x01);
		ILI9481_TFT_WR_DATA(0x3F);
		
		ILI9481_TFT_WR_REG(0x2B);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x00);
		ILI9481_TFT_WR_DATA(0x01);
		ILI9481_TFT_WR_DATA(0xE0);
		delay_ms(120);
		ILI9481_TFT_WR_REG(0x29);
		ILI9481_TFT_WR_REG(0x002c); 
	}
   ILI9481_TFT_Scan_Dir(L2R_U2D);
	ILI9481_TFT_LED=1;					//点亮背光
	ILI9481_TFT_Clear(WHITE);
}  
//清屏函数
//color:要清屏的填充色
void ILI9481_TFT_Clear(u16 color)
{
	u32 index=0;      
	u32 totalpoint=ILI9481_TFTdev.width;
	totalpoint*=ILI9481_TFTdev.height; 	//得到总点数
	ILI9481_TFT_SetCursor(0x00,0x0000);	//设置光标位置 
	ILI9481_TFT_WriteRAM_Prepare();     //开始写入GRAM	 	  
	for(index=0;index<totalpoint;index++)
	{
		ILI9481_TFT->ILI9481_TFT_RAM=color;	   
	}
}  
//在指定区域内填充单个颜色
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)   
//color:要填充的颜色
void ILI9481_TFT_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 color)
{          
	u16 i,j;
	u16 xlen=0;
	xlen=ex-sx+1;	   
	for(i=sy;i<=ey;i++)
	{
	 	ILI9481_TFT_SetCursor(sx,i);      				//设置光标位置 
		ILI9481_TFT_WriteRAM_Prepare();     			//开始写入GRAM	  
		for(j=0;j<xlen;j++)ILI9481_TFT_WR_DATA(color);	//设置光标位置 	    
	}
}  
//在指定区域内填充指定颜色块			 
//(sx,sy),(ex,ey):填充矩形对角坐标,区域大小为:(ex-sx+1)*(ey-sy+1)   
//color:要填充的颜色
void ILI9481_TFT_Color_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 *color)
{  
	u16 height,width;
	u16 i,j;
	width=ex-sx+1; 		//得到填充的宽度
	height=ey-sy+1;		//高度
 	for(i=0;i<height;i++)
	{
 		ILI9481_TFT_SetCursor(sx,sy+i);   	//设置光标位置 
		ILI9481_TFT_WriteRAM_Prepare();     //开始写入GRAM
		for(j=0;j<width;j++)ILI9481_TFT->ILI9481_TFT_RAM=color[i*height+j];//写入数据 
	}	  
}  
//画线
//x1,y1:起点坐标
//x2,y2:终点坐标  
void ILI9481_TFT_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2)
{
	u16 t; 
	int xerr=0,yerr=0,delta_x,delta_y,distance; 
	int incx,incy,uRow,uCol; 
	delta_x=x2-x1; //计算坐标增量 
	delta_y=y2-y1; 
	uRow=x1; 
	uCol=y1; 
	if(delta_x>0)incx=1; //设置单步方向 
	else if(delta_x==0)incx=0;//垂直线 
	else {incx=-1;delta_x=-delta_x;} 
	if(delta_y>0)incy=1; 
	else if(delta_y==0)incy=0;//水平线 
	else{incy=-1;delta_y=-delta_y;} 
	if( delta_x>delta_y)distance=delta_x; //选取基本增量坐标轴 
	else distance=delta_y; 
	for(t=0;t<=distance+1;t++ )//画线输出 
	{  
		ILI9481_TFT_DrawPoint(uRow,uCol);//画点 
		xerr+=delta_x ; 
		yerr+=delta_y ; 
		if(xerr>distance) 
		{ 
			xerr-=distance; 
			uRow+=incx; 
		} 
		if(yerr>distance) 
		{ 
			yerr-=distance; 
			uCol+=incy; 
		} 
	}  
}    
//画矩形	  
//(x1,y1),(x2,y2):矩形的对角坐标
void ILI9481_TFT_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2)
{
	ILI9481_TFT_DrawLine(x1,y1,x2,y1);
	ILI9481_TFT_DrawLine(x1,y1,x1,y2);
	ILI9481_TFT_DrawLine(x1,y2,x2,y2);
	ILI9481_TFT_DrawLine(x2,y1,x2,y2);
}
//在指定位置画一个指定大小的圆
//(x,y):中心点
//r    :半径
void Draw_Circle(u16 x0,u16 y0,u8 r)
{
	int a,b;
	int di;
	a=0;b=r;	  
	di=3-(r<<1);             //判断下个点位置的标志
	while(a<=b)
	{
		ILI9481_TFT_DrawPoint(x0+a,y0-b);             //5
 		ILI9481_TFT_DrawPoint(x0+b,y0-a);             //0           
		ILI9481_TFT_DrawPoint(x0+b,y0+a);             //4               
		ILI9481_TFT_DrawPoint(x0+a,y0+b);             //6 
		ILI9481_TFT_DrawPoint(x0-a,y0+b);             //1       
 		ILI9481_TFT_DrawPoint(x0-b,y0+a);             
		ILI9481_TFT_DrawPoint(x0-a,y0-b);             //2             
  		ILI9481_TFT_DrawPoint(x0-b,y0-a);             //7     	         
		a++;
		//使用Bresenham算法画圆     
		if(di<0)di +=4*a+6;	  
		else
		{
			di+=10+4*(a-b);   
			b--;
		} 						    
	}
} 									  
//在指定位置显示一个字符
//x,y:起始坐标
//num:要显示的字符:" "--->"~"
//size:字体大小 12/16
//mode:叠加方式(1)还是非叠加方式(0)
void ILI9481_TFT_ShowChar(u16 x,u16 y,u8 num,u8 size,u8 mode)
{  							  
    u8 temp,t1,t;
	u16 y0=y;
	u16 colortemp=POINT_COLOR;      			     
	//设置窗口		   
	num=num-' ';//得到偏移后的值
	if(!mode) //非叠加方式
	{
	    for(t=0;t<size;t++)
	    {   
			if(size==12)temp=asc2_1206[num][t];  //调用1206字体
			else temp=asc2_1608[num][t];		 //调用1608字体 	                          
	        for(t1=0;t1<8;t1++)
			{			    
		        if(temp&0x80)POINT_COLOR=colortemp;
				else POINT_COLOR=BACK_COLOR;
				ILI9481_TFT_DrawPoint(x,y);	
				temp<<=1;
				y++;
				if(x>=ILI9481_TFTdev.width){POINT_COLOR=colortemp;return;}//超区域了
				if((y-y0)==size)
				{
					y=y0;
					x++;
					if(x>=ILI9481_TFTdev.width){POINT_COLOR=colortemp;return;}//超区域了
					break;
				}
			}  	 
	    }    
	}else//叠加方式
	{
	    for(t=0;t<size;t++)
	    {   
			if(size==12)temp=asc2_1206[num][t];  //调用1206字体
			else temp=asc2_1608[num][t];		 //调用1608字体 	                          
	        for(t1=0;t1<8;t1++)
			{			    
		        if(temp&0x80)ILI9481_TFT_DrawPoint(x,y); 
				temp<<=1;
				y++;
				if(x>=ILI9481_TFTdev.height){POINT_COLOR=colortemp;return;}//超区域了
				if((y-y0)==size)
				{
					y=y0;
					x++;
					if(x>=ILI9481_TFTdev.width){POINT_COLOR=colortemp;return;}//超区域了
					break;
				}
			}  	 
	    }     
	}
	POINT_COLOR=colortemp;	    	   	 	  
}   
//m^n函数
//返回值:m^n次方.
u32 ILI9481_TFT_Pow(u8 m,u8 n)
{
	u32 result=1;	 
	while(n--)result*=m;    
	return result;
}			 
//显示数字,高位为0,则不显示
//x,y :起点坐标	 
//len :数字的位数
//size:字体大小
//color:颜色 
//num:数值(0~4294967295);	 
void ILI9481_TFT_ShowNum(u16 x,u16 y,u32 num,u8 len,u8 size)
{         	
	u8 t,temp;
	u8 enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/ILI9481_TFT_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				ILI9481_TFT_ShowChar(x+(size/2)*t,y,' ',size,0);
				continue;
			}else enshow=1; 
		 	 
		}
	 	ILI9481_TFT_ShowChar(x+(size/2)*t,y,temp+'0',size,0); 
	}
} 
//显示数字,高位为0,还是显示
//x,y:起点坐标
//num:数值(0~999999999);	 
//len:长度(即要显示的位数)
//size:字体大小
//mode:
//[7]:0,不填充;1,填充0.
//[6:1]:保留
//[0]:0,非叠加显示;1,叠加显示.
void ILI9481_TFT_ShowxNum(u16 x,u16 y,u32 num,u8 len,u8 size,u8 mode)
{  
	u8 t,temp;
	u8 enshow=0;						   
	for(t=0;t<len;t++)
	{
		temp=(num/ILI9481_TFT_Pow(10,len-t-1))%10;
		if(enshow==0&&t<(len-1))
		{
			if(temp==0)
			{
				if(mode&0X80)ILI9481_TFT_ShowChar(x+(size/2)*t,y,'0',size,mode&0X01);  
				else ILI9481_TFT_ShowChar(x+(size/2)*t,y,' ',size,mode&0X01);  
 				continue;
			}else enshow=1; 
		 	 
		}
	 	ILI9481_TFT_ShowChar(x+(size/2)*t,y,temp+'0',size,mode&0X01); 
	}
} 
//显示字符串
//x,y:起点坐标
//width,height:区域大小  
//size:字体大小
//*p:字符串起始地址		  
void ILI9481_TFT_ShowString(u16 x,u16 y,u16 width,u16 height,u8 size,u8 *p)
{         
	u8 x0=x;
	width+=x;
	height+=y;
    while((*p<='~')&&(*p>=' '))//判断是不是非法字符!
    {       
        if(x>=width){x=x0;y+=size;}
        if(y>=height)break;//退出
        ILI9481_TFT_ShowChar(x,y,*p,size,0);
        x+=size/2;
        p++;
    }  
}






























